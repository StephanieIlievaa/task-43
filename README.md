# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣

## HTML & CSS Task

## Objective
* Checkout the dev branch.
* Set the page title to "No new messages".
* Three seconds after the page loads set the page title to "One new message".
* When implemented merge the dev branch to master.
## Requirements
* Start the project with **npm run start**.


